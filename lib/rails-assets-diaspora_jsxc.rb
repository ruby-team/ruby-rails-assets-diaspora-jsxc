require "rails-assets-diaspora_jsxc/version"

require "rails-assets-jquery.ui"
require "rails-assets-jquery.slimscroll"
require "rails-assets-jquery-colorbox"
require "rails-assets-jquery-fullscreen-plugin"
require "rails-assets-emojione"
require "rails-assets-favico.js"

module RailsAssetsDiasporaJsxc

  def self.gem_path
    Pathname(File.realpath(__FILE__)).join('../..')
  end

  def self.gem_spec
    Gem::Specification::load(
      gem_path.join("rails-assets-diaspora_jsxc.gemspec").to_s
    )
  end

  def self.load_paths
    gem_path.join('app/assets').each_child.to_a
  end

  def self.dependencies
    [
      RailsAssetsJqueryUi,
      RailsAssetsJquerySlimscroll,
      RailsAssetsJqueryColorbox,
      RailsAssetsJqueryFullscreenPlugin,
      RailsAssetsEmojione,
      RailsAssetsFavicoJs
    ]
  end

  if defined?(Rails)
    class Engine < ::Rails::Engine
      # Rails -> use app/assets directory.
    end
  end

end

class RailsAssets
  @components ||= []

  class << self
    attr_accessor :components

    def load_paths
      components.flat_map(&:load_paths)
    end
  end
end

RailsAssets.components << RailsAssetsDiasporaJsxc
