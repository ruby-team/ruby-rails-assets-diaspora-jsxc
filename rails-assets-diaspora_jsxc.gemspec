# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails-assets-diaspora_jsxc/version'

Gem::Specification.new do |spec|
  spec.name          = "rails-assets-diaspora_jsxc"
  spec.version       = RailsAssetsDiasporaJsxc::VERSION
  spec.authors       = ["rails-assets.org"]
  spec.description   = "Real-time chat app"
  spec.summary       = "Real-time chat app"
  spec.homepage      = "https://github.com/diaspora/jsxc"
  spec.license       = "MIT"

  spec.files         = `find ./* -type f | cut -b 3-`.split($/)
  spec.require_paths = ["lib"]

  spec.add_dependency "rails-assets-jquery.ui", "~> 1.11.4"
  spec.add_dependency "rails-assets-jquery.slimscroll", "~> 1.3.6"
  spec.add_dependency "rails-assets-jquery-colorbox", "~> 1.6.3"
  spec.add_dependency "rails-assets-jquery-fullscreen-plugin", "~> 0.5.0"
  spec.add_dependency "rails-assets-emojione", "~> 2.0.1"
  spec.add_dependency "rails-assets-favico.js", ">= 0.3.10", "< 0.4"
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
